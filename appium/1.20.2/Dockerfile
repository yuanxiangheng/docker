# Copyright (c) 2021-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This Dockerfile builds on the official Appium docker image
# which is published under the Apache 2.0 license.
# https://github.com/appium/appium-docker-android
FROM appium/appium:1.20.2-p0

#============================================
# Download chromedrivers and create map.json
#============================================
RUN mkdir /chromedriver \
&&  cd /chromedriver/ \
&&  echo '{' > /chromedriver/map.json \
&&  for MAJOR_VERSION in $(seq 77 103); \
    do if [ "$MAJOR_VERSION" -eq 82 ] ; then echo "Skipping non-existant chrome 82"; continue ; fi \
&&  echo "Downloading chromedriver for chrome ${MAJOR_VERSION}" \
&&  LATEST_VERSION=$(curl -f https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${MAJOR_VERSION}) \
&&  wget -q "https://chromedriver.storage.googleapis.com/${LATEST_VERSION}/chromedriver_linux64.zip" \
&&  unzip -q chromedriver_linux64.zip \
&&  rm -f chromedriver_linux64.zip \
&&  mv chromedriver /chromedriver/${MAJOR_VERSION} \
&&  echo """\"$(/chromedriver/${MAJOR_VERSION} --version | awk {'print $2'})\": \"${MAJOR_VERSION}\",""" >> /chromedriver/map.json; \
    done \
&&  sed -i -e '$s/,//g' /chromedriver/map.json \
&&  echo '}' >> /chromedriver/map.json

#====================================================================================
# Override the TZ variables set in the appium image
# https://github.com/appium/appium-docker-android/blob/master/Appium/Dockerfile#L118
#====================================================================================
ENV TZ="Europe/Berlin"
RUN echo "${TZ}" > /etc/timezone

# Support setting APPIUM_LOG externally (https://github.com/appium/appium-docker-android/pull/138)
RUN sed -i -e 's|APPIUM_LOG="/var/log/appium.log"|APPIUM_LOG="${APPIUM_LOG:-/var/log/appium.log}"|g' /root/entry_point.sh
