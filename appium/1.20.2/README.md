# Appium

This contains the Dockerfile and associated image for running Appium in a container and connecting to a local device using adb.

It is a lightly modified version of the official Appium docker image ([github](https://github.com/appium/appium-docker-android), [docker hub](https://hub.docker.com/r/appium/appium)) which includes the chromedriver binaries pre-loaded.

## Running Appium

Appium connects to your android device using adb, either via USB or using wireless adb.

### Wireless:

Before attempting wireless adb it must first be enabled with a wired adb USB connection. See the [official docs](https://developer.android.com/studio/command-line/adb.html#wireless) but the summary is to run `adb tcpip 5555` to enable the wireless adb daemon on the device. Some android distributions (eg LineageOS) also support enabling wireless adb via the settings app.

Connect the device to the same network as the appium container and run appium as per the below command. Update the IP address in `ANDROID_DEVICES` according to your devices IP address which can be found in Settings > Wifi.

```
docker run -it --rm --name appium \
    -p 4723:4723 \
    --mount type=volume,source=android_keys,destination=/root/.android \
    -e REMOTE_ADB=true \
    -e ANDROID_DEVICES=192.168.0.133:5555 \
    -e RELAXED_SECURITY=true \
    registry.gitlab.com/eyeo/docker/appium:1.20.2
```

### Wired

Depending on the host operating system a wired adb connection from within docker can either be really easy or a complete pain. See the sections below for some tips. Once the permissions are setup the container should be started with something like..

```
docker run -it --rm --name appium \
    -p 4723:4723 \
    --mount type=volume,source=android_keys,destination=/root/.android \
    -v /dev/bus/usb:/dev/bus/usb \
    -e RELAXED_SECURITY=true \
    registry.gitlab.com/eyeo/docker/appium:1.20.2
```


## adb tips

To verify adb connectivity you can run adb from inside the container:

`docker exec -it bash`

then:

`adb devices` or `adb shell` etc.

#### USB adb - Ubuntu

Users need to be in the `plugdev` group. Installing the `adb` package on the host machine seems to install all appropriate udev rules.

#### USB adb - Fedora (tested on 31)

The appropriate udev rules should be downloaded from github..

```
git clone https://github.com/M0Rf30/android-udev-rules.git
cd android-udev-rules
sudo cp -v 51-android.rules /etc/udev/rules.d/51-android.rules
sudo chmod a+r /etc/udev/rules.d/51-android.rules
sudo groupadd adbusers
sudo usermod -a -G adbusers $(whoami)
sudo systemctl restart systemd-udevd.service
adb kill-server
```

Also note that there is an `android-tools` package which deploys `adb` as a systemd service. This should **not** be running when using the docker container.
