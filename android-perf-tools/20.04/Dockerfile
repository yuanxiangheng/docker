# Copyright (c) 2021-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:20.04

# Install job requirements and create ~/.android/ ready for keys
RUN apt-get update \
&&  apt-get install -qy --no-install-recommends \
        adb \
        curl \
        jq \
        libatomic1 \
        python3-pip \
        python3-wheel \
        python3-setuptools \
        unzip \
        wget \
        xz-utils \
        git \
        libglib2.0-0 \
        figlet \
        aapt \
        python-is-python3 \
        python2 \
        binutils \
&&  mkdir --mode=750 ~/.android/

# Install golang (the default version in the repo is too old for wpr)
RUN wget --no-verbose https://go.dev/dl/go1.17.5.linux-amd64.tar.gz \
&& echo "bd78114b0d441b029c8fe0341f4910370925a4d270a6a590668840675b0c653e  go1.17.5.linux-amd64.tar.gz" | sha256sum --check \
&& tar -C /usr/local -xzf go*.tar.gz \
&& rm go*.tar.gz

# Update PATH to include new golang directory
ENV PATH "$PATH:/usr/local/go/bin"
