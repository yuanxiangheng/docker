# yamllint Docker Images

The [yamllint](https://github.com/adrienverge/yamllint) software is a linter
for [YAML](https://yaml.org/) files.
The [Docker](https://www.docker.com/) resources herein are used to assemble
Docker container images with the yamllint software pre-installed.
