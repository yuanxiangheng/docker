# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM registry.gitlab.com/eyeo/docker/base_gitlab-runner:ubuntu-18.04

# Build dependencies for libadblockplus.
RUN apt-get update -qyy \
&&  apt-get install -qyy \
        clang \
        doxygen \
        graphviz \
        libc++-dev \
        libc++abi-dev \
&&  rm -rf /var/lib/apt/lists/*

# Install Android NDK to /third_party/
RUN mkdir /third_party/ \
&&  wget --quiet https://dl.google.com/android/repository/android-ndk-r16b-linux-x86_64.zip -O /tmp/android-ndk-16.zip \
&&  unzip -q /tmp/android-ndk-16.zip -d /third_party/ \
&&  rm /tmp/android-ndk-16.zip
